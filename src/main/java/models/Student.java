package models;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import dao.StudentDao;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "first_name")
    private String firstName;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference

    private List<Exam> exams;
    @Transient
    private double avgMarks;

    public Student() {
    }
    public void addExam(Exam exam){
        exams.add(exam);
    }

    public Student(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
        exams = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

    public double getAvgMarks() {

        StudentDao studentDao = new StudentDao();
        this.avgMarks = studentDao.getAvgMarks(this.id);

        return avgMarks;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", avgMarks=" + avgMarks +
                '}';
    }
}
