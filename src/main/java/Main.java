import input.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu();
        while (true) {
            System.out.println("Меню управление студентами : 1 \nМеню управление экзаменами : 2 \nМеню управление предметами : 3\nвыход : 0");
            Scanner scanner = new Scanner(System.in);
            scanner.useLocale(Locale.ENGLISH);
            String userInput = scanner.nextLine();
            switch (userInput){
                case "1":
                    menu.menuStudent();
                    break;
                case "2":
                    menu.menuExam();
                    break;
                case "3":
                    menu.menuSubject();
                    break;

                case "0":
                    return;

            }

        }

    }
}
