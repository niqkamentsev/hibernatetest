package input;

import models.Student;

import java.util.Comparator;
import java.util.Locale;
import java.util.Scanner;

public class UserInputSort {
    public Comparator<Student> getSortStudents(){
        System.out.println("Input how you want sort: id, name, rating");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String userInput = scanner.nextLine();

        switch (userInput){
            case "id":
                return  (o1, o2) -> Integer.compare(o1.getId(), o2.getId());
            case "name":
                return ( o1,  o2) -> o1.getFirstName().compareTo(o2.getFirstName());
            case "rating":
                return ((o1, o2) -> Double.compare(o1.getAvgMarks(),o2.getAvgMarks()));

            default:
                throw new NullPointerException();
        }

    }
}

