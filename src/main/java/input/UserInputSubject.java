package input;

import dao.SubjectDao;
import models.Subject;

import java.util.Locale;
import java.util.Scanner;

public class UserInputSubject {
    public void createSubject(){
        System.out.println("Input name SUBJECT:");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String subjectName = scanner.nextLine();
        SubjectDao subjectDao = new SubjectDao();
        Subject subject = new Subject(subjectName);
        subjectDao.saveSubject(subject);
    }
}
