package input;

import dao.StudentDao;
import json.StudentFile;
import json.StudentJson;
import models.Student;
import service.StudentService;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class UserInputStudent {
    private StudentService studentService = new StudentService();
    public void createStudentInput(){
        System.out.println("Input first name:");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String firstName = scanner.nextLine();
        System.out.println("Input last name:");
        String lastName = scanner.nextLine();
        Student student = new Student(lastName,firstName);
        studentService.saveStudent(student);
    }
    public void deleteStudentInput(){
        System.out.println("Input id Student");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        int idStudent = scanner.nextInt();


        studentService.deleteStudent(idStudent);
    }
    public void updateStudentInput(){
        System.out.println("Input id Student");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        int id = scanner.nextInt();
        System.out.println("Input new first name Student");
        String firstName = scanner.nextLine();
        System.out.println("Input new last name Student");
        String lastName = scanner.nextLine();
        studentService.updateStudent(id, firstName, lastName);

    }
    public void deleteAllStudentsInput(){
        System.out.println("Do you want delete all students? \nYes or No? ");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String answer = scanner.nextLine();

        while (true){
            if(answer.equals("Yes") || answer.equals("Y")){
                createBackupInput();
                studentService.deleteAllStudents();
                return;
            }
            else if(answer.equals("No") || answer.equals("N")){return;}

        }

    }
    public void createBackupInput(){
        System.out.println("Do you want create backup all students (Json)? \nYes or No?");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String answer = scanner.nextLine();
        while (true){
            if(answer.equals("Yes") || answer.equals("Y")){
                StudentJson studentJson = new StudentJson();
                System.out.println("Input full path");
                String path =scanner.nextLine();
                studentJson.createAllStudentJson(path);
                return;
            }
            else if(answer.equals("No") || answer.equals("N")){return;}
        }
    }

    public void getAllMarksByIdStudentInput(){
        System.out.println("Input id student");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        int idStudent = scanner.nextInt();
        List<Integer> listMarks = studentService.getAllMarks(idStudent);
        System.out.println(listMarks);
    }

    public void addStudentsThroughJson(){
        System.out.println("Input full path\n 0 exit");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String path = scanner.nextLine();
        if (path.equals("0")){
            return;
        }

        studentService.addStudentsWithJson(path);
    }
    public void createStudentsFileTxt(){
        StudentFile studentFile = new StudentFile();
        System.out.println("Input full path\n 0 exit");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String path = scanner.nextLine();
        if (path.equals("0")){
            return;
        }
        studentFile.writeToFileStudents(path);
    }
    public void readStudentsFileCSV(){
        StudentFile studentFile = new StudentFile();
        System.out.println("Input full path\n 0 exit");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        String path = scanner.nextLine();
        if (path.equals("0")){
            return;
        }
        try {
            studentFile.readFileStudents(path);
        }catch (FileNotFoundException e){
            System.out.println("Файл не найден");
        }

    }





}
