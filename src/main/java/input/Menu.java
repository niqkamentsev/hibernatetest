package input;

import json.StudentJson;
import models.Student;
import service.ExamService;
import service.StudentService;
import service.SubjectService;

import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

public class Menu {
    public void menuStudent(){
        UserInputStudent userInputStudent = new UserInputStudent();
        StudentService studentService = new StudentService();
        while (true){
            System.out.println("создать  студента 1 \nвывести всех студентов 2\nудалить студента 3 \nудалить всех студентов 4 \nполучить все оценки студента 5 \nдобавить студентов через файл Json 6\nсоздать txt файл всех студентов 7\n создать всех студентов JSON 8\n" +
                    "читать студентов CSV(только созданый этим приложением) 9\nвыход 0");
            Scanner scanner = new Scanner(System.in);
            scanner.useLocale(Locale.ENGLISH);
            String userInput = scanner.nextLine();
            switch (userInput) {
                case "1":
                    userInputStudent.createStudentInput();
                    break;
                case "2":
                    Set<Student> studentsSorted =  studentService.getAllSortStudents(new UserInputSort().getSortStudents());
                    System.out.println(studentsSorted);
                    break;
                case "3":
                    userInputStudent.deleteStudentInput();
                    break;
                case "4":
                    userInputStudent.deleteAllStudentsInput();
                    break;
                case "5":
                    userInputStudent.getAllMarksByIdStudentInput();
                    break;
                case "6":
                    userInputStudent.addStudentsThroughJson();
                    break;
                case "7":
                    userInputStudent.createStudentsFileTxt();
                    break;
                case "8":
                    userInputStudent.createBackupInput();
                    break;
                case "9":
                    userInputStudent.readStudentsFileCSV();
                    break;
                case "0":
                    return;
            }

        }
    }
    public void menuExam(){
        UserInputExam userInputExam = new UserInputExam();
        while (true){
            System.out.println("создать экзамен 1 \nудалить экзамен 2\nпоказать все экзамены 3\nвыход 0 ");
            Scanner scanner = new Scanner(System.in);
            scanner.useLocale(Locale.ENGLISH);
            String userInput = scanner.nextLine();
            switch (userInput){
                case "1":
                    userInputExam.createExam();
                    break;
                case "2":
                    userInputExam.deleteExam();
                    break;
                case "3":
                    userInputExam.printAllExam();
                case "0":
                    return;
            }

        }
    }
    public void menuSubject(){
        SubjectService subjectService = new SubjectService();
        while (true){
            System.out.println("It's alpha version \nудалить все предметы 4 \nвыход 0");
            Scanner scanner = new Scanner(System.in);
            scanner.useLocale(Locale.ENGLISH);
            String userInput = scanner.nextLine();
            switch (userInput){
                case "4":
                    subjectService.deleteAlLSubjects();
                    break;
                case "0":
                    return;

            }
        }
    }
}
