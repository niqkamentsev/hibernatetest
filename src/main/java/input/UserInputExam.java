package input;

import models.Exam;
import models.Student;
import models.Subject;
import service.ExamService;
import service.StudentService;
import service.SubjectService;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class UserInputExam {
    private ExamService examService = new ExamService();
    public void  createExam(){
        System.out.println("Input id students:");
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);
        int idStudent = scanner.nextInt();
        System.out.println("Input id subjects:");
        int idSubject = scanner.nextInt();
        System.out.println("Input id MARK:");
        int mark = scanner.nextInt();
        examService.addExam(idStudent,idSubject,mark);

    }
    public void deleteExam(){
        List<Exam> examList = examService.getAllExam();
        System.out.println(examList);
        System.out.println("Input id exam:");
        Scanner scanner = new Scanner(System.in);
        int idExam = scanner.nextInt();
        examService.deleteExam(idExam);

    }
    public void printAllExam(){
        List<Exam> examList =examService.getAllExam();
        System.out.println(examList);
    }
}
