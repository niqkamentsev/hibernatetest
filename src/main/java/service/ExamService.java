package service;

import dao.ExamDao;
import dao.StudentDao;
import dao.SubjectDao;
import models.Exam;
import models.Student;
import models.Subject;

import java.util.List;

public class ExamService {
    private ExamDao examDao =  new ExamDao();

    public void addExam(int idStudent, int idSubject, int mark){
        Exam exam = new Exam();
        StudentDao studentDao = new StudentDao();
        Student studentId = studentDao.findById(idStudent);
        SubjectDao subjectDao = new SubjectDao();
        Subject subjectId = subjectDao.findSubjectById(idSubject);
        exam.setStudent(studentId);
        exam.setSubject(subjectId);
        exam.setMarks(mark);
        examDao.addExam(exam);
    }
    public void deleteExam(int idExam){
        Exam exam = examDao.findExamById(idExam);
        examDao.deleteExam(exam);
    }
    public List<Exam> getAllExam(){
        return examDao.getAllExam();
    }

}
