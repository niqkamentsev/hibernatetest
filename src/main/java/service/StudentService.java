package service;

import dao.ExamDao;
import dao.StudentDao;
import dao.SubjectDao;

import json.StudentJson;
import models.Student;
import models.Subject;


import java.util.*;

public class StudentService {
    private StudentDao studentDao = new StudentDao();


    public Student findById(int id){
        return studentDao.findById(id);
    }
    public void saveStudent(Student student){
        SubjectDao subjectDao = new SubjectDao();
        List<Subject> subjects = subjectDao.getAllSubjects();
        ExamService examService = new ExamService();
        studentDao.saveStudent(student);
        int idStudent = student.getId();
        for (Subject s :subjects){
            examService.addExam(idStudent,s.getId(),0);
        }
    }

    public void  addStudentsWithJson(String path){
        StudentJson studentJson = new StudentJson();
        List<Student> studentList = studentJson.readStudentsJson(path);
        for(Student student :studentList){
            studentDao.saveStudentBackUp(student);
        }

    }
    public void updateStudent(int id,String firstName, String lastName){
        Student student = new Student(lastName, firstName);
        student.setId(id);
        studentDao.updateStudent(student);
    }


    public void deleteStudent(int  id){
        studentDao.deleteStudent(id);
    }


    public SortedSet<Student> getAllSortStudents(Comparator<Student> comparator){
        SortedSet<Student> students = new TreeSet<>(comparator);
        students.addAll(studentDao.getAllStudents());
        return students;

    }

    public void deleteAllStudents(){
        studentDao.deleteAllStudents();
    }
    public List<Student> getAllStudents(){
        return studentDao.getAllStudents();
    }
    public List<Integer> getAllMarks(int id){
        return studentDao.getAllMarks(id);
    }

    public double getAvgMark(int id){
        return studentDao.getAvgMarks(id);
    }


}
