package service;

import dao.SubjectDao;
import models.Subject;

import java.util.List;

public class SubjectService {
    SubjectDao subjectDao = new SubjectDao();


    public Subject findSubject(int id){
        return subjectDao.findSubjectById(id);
    }
    public Subject findSubject(String name){
        return subjectDao.findSubjectByName(name);
    }
    public void saveSubject(Subject subject){
        subjectDao.saveSubject(subject);
    }
    public void updateSubject(Subject subject){
        subjectDao.updateSubject(subject);
    }
    public void deleteSubject(Subject subject){
        subjectDao.deleteSubject(subject);
    }
    public void deleteAlLSubjects(){
        subjectDao.deleteAllSubjects();
    }
    public List<Subject> getAllSubject(){
        return subjectDao.getAllSubjects();
    }

}
