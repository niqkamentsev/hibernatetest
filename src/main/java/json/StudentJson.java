package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Student;
import service.StudentService;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class StudentJson {
    private ObjectMapper objectMapper = new ObjectMapper();

    public void createStudentJson(String path , int idStudent){
        StudentService studentService = new StudentService();
        Student student = studentService.findById(idStudent);
        try{
            objectMapper.writeValue(new File(path), student);
        }catch (IOException e){
            System.out.println(e);
        }
    }
    public void createAllStudentJson(String path){
        StudentService studentService = new StudentService();
        List<Student> studentList = studentService.getAllStudents();
        try {
            objectMapper.writeValue(new File(path),studentList);
        }catch (IOException e){
            System.out.println(e);
        }
    }
    public List<Student> readStudentsJson(String path){
        try {
            return Arrays.asList(objectMapper.readValue(new File(path), Student[].class));
        }catch (IOException e){
            System.out.println(e);
            return null;
        }

    }
    public Student readStudentJson(String path){
        try {
            return objectMapper.readValue(new File(path), Student.class);
        }catch (IOException e){
            System.out.println(e);
            return null;
        }

    }

}
