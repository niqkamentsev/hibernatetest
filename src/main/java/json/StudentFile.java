package json;


import models.Student;
import service.StudentService;

import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class StudentFile {
    private StudentService studentService = new StudentService();


    public void writeToFileStudents(String path){
        String content = "id,first name,last Name\n";
        List<Student> studentList = studentService.getAllStudents();
        try (FileWriter writer = new FileWriter(path);
             BufferedWriter bw = new BufferedWriter(writer)) {
                bw.write(content);
                for(Student student :studentList){
                    bw.write(student.getId() + ",");
                    bw.write(student.getFirstName() + ",");
                    bw.write(student.getLastName() + ",\n");
                }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }
    public void readFileStudents(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner readerFile = new Scanner(file);
        readerFile.useDelimiter(",");
        boolean flag= true;
        Student student = new Student();
        while (readerFile.hasNextLine()){
            if (flag){
                readerFile.nextLine();
                flag = false;
                continue;
            }
            student.setId(readerFile.nextInt());
            student.setFirstName(readerFile.next());
            student.setLastName(readerFile.next());
            readerFile.nextLine();
            System.out.println(student);


        }



    }


}
