package dao;

import models.Exam;
import models.Student;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import service.ExamService;
import utils.HibernateSessionFactoryUtil;

import java.util.*;

public class StudentDao {
    public Student findById(int id){
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Student.class, id);
    }
    public void saveStudent(Student student){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(student);
        tx1.commit();
        session.close();
    }
    public void saveStudentBackUp(Student student){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.replicate(student,ReplicationMode.EXCEPTION );
        tx1.commit();
        session.close();
    }

    public void updateStudent(Student student){
        Session session =HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(student);
        tx1.commit();
        session.close();
    }
    public void deleteStudent(int  id ){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("delete Student c  where c.id = " + id ).executeUpdate();
        tx1.commit();
        session.close();
    }
    public void deleteAllStudents(){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("delete from Student ").executeUpdate();
        session.createSQLQuery("alter table students AUTO_INCREMENT = 1").executeUpdate();
        session.createSQLQuery("alter table exams  AUTO_INCREMENT = 1").executeUpdate();
        tx1.commit();
        session.close();
    }
    public List<Student> getAllStudents( ){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Student> students = session.createQuery("from Student ", Student.class).list();
        session.close();
        return students;
    }
    public List<Integer> getAllMarks(int id ){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Integer> marks = session.createQuery("select marks from Exam WHERE student.id = " + id).list();
        session.close();
        return marks;
    }
    public double getAvgMarks (int id){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query<Double> query = session.createQuery("select avg (marks) from Exam where student.id = ?1 and  not marks = 0 ");
        query.setParameter(1, id);
        if(query.uniqueResult() == null){
            return 0;
        }
        return (double) query.uniqueResult();


    }

}
