package dao;

import models.Exam;

import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

public class ExamDao {
    public void addExam(Exam exam){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(exam);
        tx1.commit();
        session.close();
    }
    public void updateExam (int idExam ,int mark){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("update Exam c set c.marks = ?1 where c.id = ?2 ").setParameter(1, mark).setParameter(2,idExam).executeUpdate();
        tx1.commit();
        session.close();
    }
    public Exam findExamById(int id){
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Exam.class, id);
    }
    public void deleteExam(Exam exam){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(exam);
        tx1.commit();
        session.close();
    }

    public List<Exam> getAllExam( ){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Exam> students = session.createQuery("from Exam ", Exam.class).list();
        session.close();
        return students;
    }




}
