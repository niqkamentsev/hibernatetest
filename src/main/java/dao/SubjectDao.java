package dao;

import models.Subject;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateSessionFactoryUtil;


import java.util.List;

public class SubjectDao {
    public Subject findSubjectById(int id){
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Subject.class, id);
    }
    public Subject findSubjectByName(String name){
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("from Subject where nameSubject = ?1 ");
        query.setParameter(1, name);
        return (Subject) query.uniqueResult();
    }

    public void saveSubject(Subject subject){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(subject);
        tx1.commit();
        session.close();
    }
    public void updateSubject(Subject subject){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(subject);
        tx1.commit();
        session.close();
    }
    public void deleteSubject(Subject subject){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(subject);
        tx1.commit();
        session.close();
    }
    public List<Subject> getAllSubjects(){
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("from Subject ", Subject.class).list();
    }
    public void deleteAllSubjects(){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("delete from Subject ").executeUpdate();
        session.createSQLQuery("alter table subjects AUTO_INCREMENT = 1").executeUpdate();
        tx1.commit();
        session.close();
    }
}
